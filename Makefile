build:
	go build -o api internal/cmd/api/main.go

build-cli:
	go build -o cli internal/cmd/cli/main.go

test:
	 go test --short -v --cover -race -count 100  ./...

test-full:
	go clean -testcache ./... && go test  ./...


test-integration:
	set -e ;\
	docker-compose -f docker-compose.yml up --build -d ;\
	test_status_code=0 ;\
	docker-compose -f docker-compose.yml run integration_tests go test || test_status_code=$$? ;\
	docker-compose -f docker-compose.yml down ;\
	exit $$test_status_code ;

test-cleanup:
	docker-compose -f docker-compose.yml down \
        --rmi local \
		--volumes \
		--remove-orphans \
		--timeout 60; \
  	docker-compose rm -f

up:
	docker-compose -f docker-compose.yml up --build -d

down:
	docker-compose -f docker-compose.yml down -v

grpc-api:
	protoc --go_out=. --go-grpc_out=. grpc/abf.proto

run:
	go build -o api internal/cmd/api/main.go && CONFIGOR_DEBUG_MODE=true ./api

generate:
	go generate generate.go