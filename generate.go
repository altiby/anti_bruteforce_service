package main

// there is gomock generate instructions
//nolint:lll    //go:generate mockgen -source=internal/view/grpc/abf/server.go -package=abf -destination=internal/view/grpc/abf/server_mock.go AntiBruteforceService
//nolint:lll	//go:generate mockgen -source=internal/view/grpc/abf/server.go -package=abf -destination=internal/view/grpc/abf/server_mock.go IPListService
//nolint:lll	//go:generate mockgen -source=internal/iplist/service.go -package=iplist -destination=internal/iplist/service_mock.go Storage
//nolint:lll 	//go:generate mockgen -source=internal/anti_brutforce/service.go -package=anti_brutforce -destination=internal/anti_brutforce/service_mock.go KVStorage
