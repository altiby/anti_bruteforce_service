module bitbucket.org/altiby/anti_bruteforce_service/tests

go 1.16

require (
	bitbucket.org/altiby/anti_bruteforce_service v0.0.0-20211201123231-3e87f218ac26
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0
	google.golang.org/grpc v1.42.0
	google.golang.org/protobuf v1.27.1
)
