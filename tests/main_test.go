package scripts

import (
	"context"
	"os"
	"testing"
	"time"

	"bitbucket.org/altiby/anti_bruteforce_service/internal/view/grpc/abf"
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/emptypb"
)

const delay = 5 * time.Second

var grpcClient abf.ABFServiceClient

func TestMain(m *testing.M) {
	logrus.Infof("wait %s for service availability...", delay)
	time.Sleep(delay)

	abfServiceConnection, err := grpc.Dial("api:9090",
		grpc.WithDisableRetry(),
		grpc.WithInsecure(),
		grpc.WithBlock(),
	)
	if err != nil {
		logrus.Fatal(err)
	}
	grpcClient = abf.NewABFServiceClient(abfServiceConnection)
	defer abfServiceConnection.Close()

	status := 0

	if st := m.Run(); st > status {
		status = st
	}
	os.Exit(status)
}

func Test(t *testing.T) {

	t.Run("b/w lists", func(t *testing.T) {
		_, err := grpcClient.AddToBlackList(context.Background(), &abf.IPRequest{Ip: "192.168.1.0/24"})
		require.NoError(t, err)

		_, err = grpcClient.AddToWhiteList(context.Background(), &abf.IPRequest{Ip: "172.16.1.0/24"})
		require.NoError(t, err)

		blist, err := grpcClient.LSBlackList(context.Background(), &emptypb.Empty{})
		require.NoError(t, err)
		require.Len(t, blist.Ip, 1)

		wlist, err := grpcClient.LSBWhiteList(context.Background(), &emptypb.Empty{})
		require.NoError(t, err)
		require.Len(t, wlist.Ip, 1)

		//white list
		for i := 0; i < 10000; i++ {
			resp, err := grpcClient.CanLogin(context.Background(), &abf.LoginRequest{
				Login:    "login",
				Password: "pass",
				Ip:       "172.16.1.23",
			})
			require.NoError(t, err)
			require.True(t, resp.Ok)
		}

		//black list
		for i := 0; i < 10000; i++ {
			resp, err := grpcClient.CanLogin(context.Background(), &abf.LoginRequest{
				Login:    "login",
				Password: "pass",
				Ip:       "192.168.1.23",
			})
			require.NoError(t, err)
			require.False(t, resp.Ok)
		}
	})

	t.Run("limit by username", func(t *testing.T) {
		//limit by username
		for i := 0; i < 10; i++ {
			resp, err := grpcClient.CanLogin(context.Background(), &abf.LoginRequest{
				Login:    "login",
				Password: uuid.NewV4().String(),
				Ip:       uuid.NewV4().String(),
			})
			require.NoError(t, err)
			require.True(t, resp.Ok)
		}

		resp, err := grpcClient.CanLogin(context.Background(), &abf.LoginRequest{
			Login:    "login",
			Password: uuid.NewV4().String(),
			Ip:       uuid.NewV4().String(),
		})
		require.NoError(t, err)
		require.False(t, resp.Ok)
	})

	t.Run("limit by password", func(t *testing.T) {
		//limit by password
		for i := 0; i < 100; i++ {
			resp, err := grpcClient.CanLogin(context.Background(), &abf.LoginRequest{
				Login:    uuid.NewV4().String(),
				Password: "password",
				Ip:       uuid.NewV4().String(),
			})
			require.NoError(t, err)
			require.True(t, resp.Ok)
		}

		resp, err := grpcClient.CanLogin(context.Background(), &abf.LoginRequest{
			Login:    uuid.NewV4().String(),
			Password: "password",
			Ip:       uuid.NewV4().String(),
		})

		require.NoError(t, err)
		require.False(t, resp.Ok)

	})

	t.Run("limit by ip", func(t *testing.T) {
		//limit by password
		for i := 0; i < 1000; i++ {
			resp, err := grpcClient.CanLogin(context.Background(), &abf.LoginRequest{
				Login:    uuid.NewV4().String(),
				Password: uuid.NewV4().String(),
				Ip:       "ip",
			})
			require.NoError(t, err)
			require.True(t, resp.Ok)
		}

		resp, err := grpcClient.CanLogin(context.Background(), &abf.LoginRequest{
			Login:    uuid.NewV4().String(),
			Password: uuid.NewV4().String(),
			Ip:       "ip",
		})

		require.NoError(t, err)
		require.False(t, resp.Ok)

	})

}
