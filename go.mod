module bitbucket.org/altiby/anti_bruteforce_service

go 1.16

require (
	github.com/Nvveen/Gotty v0.0.0-20120604004816-cd527374f1e5 // indirect
	github.com/antelman107/net-wait-go v0.0.0-20210623112055-cf684aebda7b
	github.com/cenkalti/backoff v2.2.1+incompatible // indirect
	github.com/docker/docker v20.10.9+incompatible
	github.com/go-redis/redis/v8 v8.11.4
	github.com/golang-migrate/migrate/v4 v4.15.1
	github.com/golang/mock v1.6.0
	github.com/gotestyourself/gotestyourself v2.2.0+incompatible // indirect
	github.com/grpc-ecosystem/go-grpc-middleware v1.0.1-0.20190118093823-f849b5445de4
	github.com/jinzhu/configor v1.2.1
	github.com/jmoiron/sqlx v1.3.4
	github.com/lib/pq v1.10.4
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mgutz/ansi v0.0.0-20200706080929-d51e80ef957d // indirect
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.17.0 // indirect
	github.com/ory/dockertest v3.3.5+incompatible
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0
	github.com/urfave/cli/v2 v2.3.0
	github.com/x-cray/logrus-prefixed-formatter v0.5.2
	google.golang.org/grpc v1.42.0
	google.golang.org/protobuf v1.27.1
)
