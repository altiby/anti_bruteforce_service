create table blacklist
(
    ip         text                               not null primary key,
    updated_at timestamp,
    created_at timestamp default CURRENT_TIMESTAMP not null
);

create table whitelist
(
    ip         text                                not null primary key,
    updated_at timestamp,
    created_at timestamp default CURRENT_TIMESTAMP not null
);


-- +migrate StatementBegin
create function update_timestamp() returns trigger
    language plpgsql
as
$$
BEGIN
    NEW.updated_at = NOW();
    RETURN NEW;
END;
$$;
-- +migrate StatementEnd

create trigger blacklist_on_update
    before insert or update
    on blacklist
    for each row
execute procedure update_timestamp();

create trigger whitelist_on_update
    before insert or update
    on whitelist
    for each row
execute procedure update_timestamp();



