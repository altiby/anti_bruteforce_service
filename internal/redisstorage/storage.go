package redisstorage

import (
	"context"
	"errors"
	"time"

	"github.com/go-redis/redis/v8"
)

var (
	ErrExpireTimeNotSet = errors.New("expire time did not set")
	RecordTTL           = 60 * time.Second
)

type Storage struct {
	rdb *redis.Client
}

func NewRDBStorage(rdb *redis.Client) *Storage {
	return &Storage{rdb: rdb}
}

func (s *Storage) IncKey(ctx context.Context, key string) (int64, error) {
	res, err := s.rdb.Incr(ctx, key).Result()
	if err != nil {
		return 0, err
	}

	ok, err := s.rdb.Expire(ctx, key, RecordTTL).Result()
	if err != nil {
		return 0, err
	}
	if !ok {
		return 0, ErrExpireTimeNotSet
	}

	return res, nil
}

func (s *Storage) SetKey(ctx context.Context, key string, value int64) error {
	_, err := s.rdb.Set(ctx, key, value, RecordTTL).Result()
	return err
}
