package antibruteforce

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
)

const (
	ipm int64 = 1000
	upm int64 = 10
	ppm int64 = 100
)

func TestService_CanLogin(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	ip := "192.168.10.1"
	user := "user"
	password := "password"

	t.Run("all limit is ok", func(t *testing.T) {
		storage := NewMockKVStorage(mockCtrl)
		service := NewAntiBrutForcesService(storage, ipm, upm, ppm)
		storage.EXPECT().IncKey(context.Background(), service.createUserKey(user)).Return(upm, nil).AnyTimes()
		storage.EXPECT().IncKey(context.Background(), service.createPasswordKey(password)).Return(ppm, nil).AnyTimes()
		storage.EXPECT().IncKey(context.Background(), service.createIPKey(ip)).Return(ipm, nil).AnyTimes()
		res, err := service.CanLogin(context.Background(), ip, user, password)
		require.NoError(t, err)
		require.True(t, res)
	})

	t.Run("ip limit reached", func(t *testing.T) {
		storage := NewMockKVStorage(mockCtrl)
		service := NewAntiBrutForcesService(storage, ipm, upm, ppm)
		storage.EXPECT().IncKey(context.Background(), service.createUserKey(user)).Return(upm, nil).AnyTimes()
		storage.EXPECT().IncKey(context.Background(), service.createPasswordKey(password)).Return(ppm, nil).AnyTimes()
		storage.EXPECT().IncKey(context.Background(), service.createIPKey(ip)).Return(ipm+1, nil).AnyTimes()
		res, err := service.CanLogin(context.Background(), ip, user, password)
		require.NoError(t, err)
		require.False(t, res)
	})

	t.Run("user limit reached", func(t *testing.T) {
		storage := NewMockKVStorage(mockCtrl)
		service := NewAntiBrutForcesService(storage, ipm, upm, ppm)
		storage.EXPECT().IncKey(context.Background(), service.createUserKey(user)).Return(upm+1, nil).AnyTimes()
		storage.EXPECT().IncKey(context.Background(), service.createPasswordKey(password)).Return(ppm, nil).AnyTimes()
		storage.EXPECT().IncKey(context.Background(), service.createIPKey(ip)).Return(ipm, nil).AnyTimes()
		res, err := service.CanLogin(context.Background(), ip, user, password)
		require.NoError(t, err)
		require.False(t, res)
	})

	t.Run("password limit reached", func(t *testing.T) {
		storage := NewMockKVStorage(mockCtrl)
		service := NewAntiBrutForcesService(storage, ipm, upm, ppm)
		storage.EXPECT().IncKey(context.Background(), service.createUserKey(user)).Return(upm, nil).AnyTimes()
		storage.EXPECT().IncKey(context.Background(), service.createPasswordKey(password)).Return(ppm+1, nil).AnyTimes()
		storage.EXPECT().IncKey(context.Background(), service.createIPKey(ip)).Return(ipm, nil).AnyTimes()
		res, err := service.CanLogin(context.Background(), ip, user, password)
		require.NoError(t, err)
		require.False(t, res)
	})

	t.Run("user and password limit reached", func(t *testing.T) {
		storage := NewMockKVStorage(mockCtrl)
		service := NewAntiBrutForcesService(storage, ipm, upm, ppm)
		storage.EXPECT().IncKey(context.Background(), service.createUserKey(user)).Return(upm+1, nil).AnyTimes()
		storage.EXPECT().IncKey(context.Background(), service.createPasswordKey(password)).Return(ppm+1, nil).AnyTimes()
		storage.EXPECT().IncKey(context.Background(), service.createIPKey(ip)).Return(ipm, nil).AnyTimes()
		res, err := service.CanLogin(context.Background(), ip, user, password)
		require.NoError(t, err)
		require.False(t, res)
	})
}
