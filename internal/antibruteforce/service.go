package antibruteforce

import (
	"context"
	"fmt"
	"time"
)

type KVStorage interface {
	IncKey(ctx context.Context, key string) (int64, error)
	SetKey(ctx context.Context, key string, value int64) error
}

type Service struct {
	storage                  KVStorage
	onIPPerMinuteCount       int64
	onUserPerMinuteCount     int64
	onPasswordPerMinuteCount int64
}

func NewAntiBrutForcesService(storage KVStorage,
	onIPPerMinuteCount int64,
	onUserPerMinuteCount int64,
	onPasswordPerMinuteCount int64) *Service {
	return &Service{
		storage:                  storage,
		onIPPerMinuteCount:       onIPPerMinuteCount,
		onUserPerMinuteCount:     onUserPerMinuteCount,
		onPasswordPerMinuteCount: onPasswordPerMinuteCount,
	}
}

func (s *Service) createKey(prefix, data string) string {
	return fmt.Sprintf("%s_%s_%s", prefix, data, time.Now().UTC().Format("1504"))
}

func (s *Service) createUserKey(userName string) string {
	return s.createKey("user", userName)
}

func (s *Service) createPasswordKey(password string) string {
	return s.createKey("password", password)
}

func (s *Service) createIPKey(ip string) string {
	return s.createKey("ip", ip)
}

func (s *Service) CanLogin(ctx context.Context, ip string, userName string, password string) (bool, error) {
	ipKey := s.createIPKey(ip)
	ipCount, err := s.storage.IncKey(ctx, ipKey)
	if err != nil {
		return false, err
	}
	if ipCount > s.onIPPerMinuteCount {
		return false, nil
	}

	userKey := s.createUserKey(userName)
	userCount, err := s.storage.IncKey(ctx, userKey)
	if err != nil {
		return false, err
	}
	if userCount > s.onUserPerMinuteCount {
		return false, nil
	}

	passwordKey := s.createPasswordKey(password)
	passwordCount, err := s.storage.IncKey(ctx, passwordKey)
	if err != nil {
		return false, err
	}
	if passwordCount > s.onPasswordPerMinuteCount {
		return false, nil
	}
	return true, nil
}

func (s *Service) ClearBuckets(ctx context.Context, userName string, password string) error {
	err := s.storage.SetKey(ctx, s.createUserKey(userName), 0)
	if err != nil {
		return err
	}

	err = s.storage.SetKey(ctx, s.createPasswordKey(password), 0)
	if err != nil {
		return err
	}

	return nil
}
