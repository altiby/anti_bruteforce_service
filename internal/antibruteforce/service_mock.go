// Code generated by MockGen. DO NOT EDIT.
// Source: internal/anti_brutforce/service.go

// Package anti_brutforce is a generated GoMock package.
package antibruteforce

import (
	context "context"
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
)

// MockKVStorage is a mock of KVStorage interface
type MockKVStorage struct {
	ctrl     *gomock.Controller
	recorder *MockKVStorageMockRecorder
}

// MockKVStorageMockRecorder is the mock recorder for MockKVStorage
type MockKVStorageMockRecorder struct {
	mock *MockKVStorage
}

// NewMockKVStorage creates a new mock instance
func NewMockKVStorage(ctrl *gomock.Controller) *MockKVStorage {
	mock := &MockKVStorage{ctrl: ctrl}
	mock.recorder = &MockKVStorageMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use
func (m *MockKVStorage) EXPECT() *MockKVStorageMockRecorder {
	return m.recorder
}

// IncKey mocks base method
func (m *MockKVStorage) IncKey(ctx context.Context, key string) (int64, error) {
	ret := m.ctrl.Call(m, "IncKey", ctx, key)
	ret0, _ := ret[0].(int64)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// IncKey indicates an expected call of IncKey
func (mr *MockKVStorageMockRecorder) IncKey(ctx, key interface{}) *gomock.Call {
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "IncKey", reflect.TypeOf((*MockKVStorage)(nil).IncKey), ctx, key)
}

// SetKey mocks base method
func (m *MockKVStorage) SetKey(ctx context.Context, key string, value int64) error {
	ret := m.ctrl.Call(m, "SetKey", ctx, key, value)
	ret0, _ := ret[0].(error)
	return ret0
}

// SetKey indicates an expected call of SetKey
func (mr *MockKVStorageMockRecorder) SetKey(ctx, key, value interface{}) *gomock.Call {
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SetKey", reflect.TypeOf((*MockKVStorage)(nil).SetKey), ctx, key, value)
}
