package abf

import (
	"context"

	"google.golang.org/protobuf/types/known/emptypb"
)

type Server struct {
	UnimplementedABFServiceServer
	abfService AntiBruteforceService
	blackList  IPListService
	whiteList  IPListService
}

func NewABFServer(abfService AntiBruteforceService, blackList IPListService, whiteList IPListService) *Server {
	return &Server{abfService: abfService, blackList: blackList, whiteList: whiteList}
}

type AntiBruteforceService interface {
	CanLogin(ctx context.Context, ip string, userName string, password string) (bool, error)
	ClearBuckets(ctx context.Context, userName string, password string) error
}

type IPListService interface {
	Add(ctx context.Context, ip string) error
	Delete(ctx context.Context, ip string) error
	IPInList(ctx context.Context, ip string) (bool, error)
	LS(ctx context.Context) ([]string, error)
}

func (s *Server) CanLogin(ctx context.Context, loginRequest *LoginRequest) (*LoginResponse, error) {
	iwl, err := s.whiteList.IPInList(ctx, loginRequest.Ip)
	if iwl {
		return &LoginResponse{Ok: iwl}, err
	}

	ibl, err := s.blackList.IPInList(ctx, loginRequest.Ip)
	if ibl {
		return &LoginResponse{Ok: !ibl}, err
	}

	isCan, err := s.abfService.CanLogin(ctx, loginRequest.Ip, loginRequest.Login, loginRequest.Password)
	return &LoginResponse{Ok: isCan}, err
}

func (s *Server) ClearBucket(ctx context.Context, clearBucketRequest *ClearBucketRequest) (*emptypb.Empty, error) {
	err := s.abfService.ClearBuckets(ctx, clearBucketRequest.Login, clearBucketRequest.Password)
	return &emptypb.Empty{}, err
}

func (s *Server) AddToBlackList(ctx context.Context, ipRequest *IPRequest) (*emptypb.Empty, error) {
	err := s.blackList.Add(ctx, ipRequest.Ip)
	return &emptypb.Empty{}, err
}

func (s *Server) DelFromBlackList(ctx context.Context, ipRequest *IPRequest) (*emptypb.Empty, error) {
	err := s.blackList.Delete(ctx, ipRequest.Ip)
	return &emptypb.Empty{}, err
}

func (s *Server) LSBlackList(ctx context.Context, e *emptypb.Empty) (*ListIP, error) {
	list, err := s.blackList.LS(ctx)
	return &ListIP{Ip: list}, err
}

func (s *Server) AddToWhiteList(ctx context.Context, ipRequest *IPRequest) (*emptypb.Empty, error) {
	err := s.whiteList.Add(ctx, ipRequest.Ip)
	return &emptypb.Empty{}, err
}

func (s *Server) DelFromWhiteList(ctx context.Context, ipRequest *IPRequest) (*emptypb.Empty, error) {
	err := s.whiteList.Delete(ctx, ipRequest.Ip)
	return &emptypb.Empty{}, err
}

func (s *Server) LSBWhiteList(ctx context.Context, e *emptypb.Empty) (*ListIP, error) {
	list, err := s.whiteList.LS(ctx)
	return &ListIP{Ip: list}, err
}
