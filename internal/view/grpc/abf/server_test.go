package abf

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
)

func TestServer_CanLogin(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	t.Run("white list", func(t *testing.T) {
		abfService := NewMockAntiBruteforceService(mockCtrl)
		wlService := NewMockIPListService(mockCtrl)
		blService := NewMockIPListService(mockCtrl)
		server := NewABFServer(abfService, blService, wlService)

		req := &LoginRequest{
			Login:    "user",
			Password: "password",
			Ip:       "192.168.0.1",
		}

		wlService.EXPECT().IPInList(context.Background(), req.Ip).Return(true, nil).AnyTimes()
		blService.EXPECT().IPInList(context.Background(), req.Ip).Return(false, nil).AnyTimes()

		response, err := server.CanLogin(context.Background(), req)
		require.NoError(t, err)
		require.Equal(t, true, response.Ok)
	})

	t.Run("black list", func(t *testing.T) {
		abfService := NewMockAntiBruteforceService(mockCtrl)
		wlService := NewMockIPListService(mockCtrl)
		blService := NewMockIPListService(mockCtrl)
		server := NewABFServer(abfService, blService, wlService)

		req := &LoginRequest{
			Login:    "user",
			Password: "password",
			Ip:       "192.168.1.1",
		}

		wlService.EXPECT().IPInList(context.Background(), req.Ip).Return(false, nil).AnyTimes()
		blService.EXPECT().IPInList(context.Background(), req.Ip).Return(true, nil).AnyTimes()

		response, err := server.CanLogin(context.Background(), req)
		require.NoError(t, err)
		require.Equal(t, false, response.Ok)
	})

	t.Run("can login", func(t *testing.T) {
		abfService := NewMockAntiBruteforceService(mockCtrl)
		wlService := NewMockIPListService(mockCtrl)
		blService := NewMockIPListService(mockCtrl)
		server := NewABFServer(abfService, blService, wlService)

		req := &LoginRequest{
			Login:    "user",
			Password: "password",
			Ip:       "192.168.1.1",
		}

		wlService.EXPECT().IPInList(context.Background(), req.Ip).Return(false, nil).AnyTimes()
		blService.EXPECT().IPInList(context.Background(), req.Ip).Return(false, nil).AnyTimes()
		abfService.EXPECT().CanLogin(context.Background(), req.Ip, req.Login, req.Password).Return(true, nil)

		response, err := server.CanLogin(context.Background(), req)
		require.NoError(t, err)
		require.Equal(t, true, response.Ok)
	})
}

func TestServer_ClearBucket(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	abfService := NewMockAntiBruteforceService(mockCtrl)
	bwlService := NewMockIPListService(mockCtrl)
	server := NewABFServer(abfService, bwlService, bwlService)

	req := &ClearBucketRequest{
		Login:    "admin",
		Password: "admin",
	}
	abfService.EXPECT().ClearBuckets(context.Background(), req.Login, req.Password).Return(nil).AnyTimes()

	_, err := server.ClearBucket(context.Background(), req)
	require.NoError(t, err)
}

func TestServer_AddToIPList(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	abfService := NewMockAntiBruteforceService(mockCtrl)
	bwlService := NewMockIPListService(mockCtrl)
	req := &IPRequest{Ip: "192.168.1.0/24"}
	bwlService.EXPECT().Add(context.Background(), req.Ip).Return(nil).AnyTimes()

	server := NewABFServer(abfService, bwlService, bwlService)
	_, err := server.AddToBlackList(context.Background(), req)
	require.NoError(t, err)

	_, err = server.AddToWhiteList(context.Background(), req)
	require.NoError(t, err)
}

func TestServer_DelFromList(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	abfService := NewMockAntiBruteforceService(mockCtrl)
	bwlService := NewMockIPListService(mockCtrl)
	req := &IPRequest{Ip: "192.168.1.0/24"}
	bwlService.EXPECT().Delete(context.Background(), req.Ip).Return(nil).AnyTimes()

	server := NewABFServer(abfService, bwlService, bwlService)

	_, err := server.DelFromBlackList(context.Background(), req)
	require.NoError(t, err)

	_, err = server.DelFromWhiteList(context.Background(), req)
	require.NoError(t, err)
}

func TestServer_LSList(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	abfService := NewMockAntiBruteforceService(mockCtrl)
	bwlService := NewMockIPListService(mockCtrl)
	ipList := []string{"1", "2", "3"}
	bwlService.EXPECT().LS(context.Background()).Return(ipList, nil).AnyTimes()

	server := NewABFServer(abfService, bwlService, bwlService)

	list, err := server.LSBlackList(context.Background(), nil)
	require.NoError(t, err)
	require.Equal(t, ipList, list.Ip)
}
