// Code generated by MockGen. DO NOT EDIT.
// Source: internal/iplist/service.go

// Package iplist is a generated GoMock package.
package iplist

import (
	context "context"
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
)

// MockStorage is a mock of Storage interface
type MockStorage struct {
	ctrl     *gomock.Controller
	recorder *MockStorageMockRecorder
}

// MockStorageMockRecorder is the mock recorder for MockStorage
type MockStorageMockRecorder struct {
	mock *MockStorage
}

// NewMockStorage creates a new mock instance
func NewMockStorage(ctrl *gomock.Controller) *MockStorage {
	mock := &MockStorage{ctrl: ctrl}
	mock.recorder = &MockStorageMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use
func (m *MockStorage) EXPECT() *MockStorageMockRecorder {
	return m.recorder
}

// Add mocks base method
func (m *MockStorage) Add(ctx context.Context, ip string) error {
	ret := m.ctrl.Call(m, "Add", ctx, ip)
	ret0, _ := ret[0].(error)
	return ret0
}

// Add indicates an expected call of Add
func (mr *MockStorageMockRecorder) Add(ctx, ip interface{}) *gomock.Call {
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Add", reflect.TypeOf((*MockStorage)(nil).Add), ctx, ip)
}

// Delete mocks base method
func (m *MockStorage) Delete(ctx context.Context, ip string) error {
	ret := m.ctrl.Call(m, "Delete", ctx, ip)
	ret0, _ := ret[0].(error)
	return ret0
}

// Delete indicates an expected call of Delete
func (mr *MockStorageMockRecorder) Delete(ctx, ip interface{}) *gomock.Call {
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Delete", reflect.TypeOf((*MockStorage)(nil).Delete), ctx, ip)
}

// LS mocks base method
func (m *MockStorage) LS(ctx context.Context) ([]string, error) {
	ret := m.ctrl.Call(m, "LS", ctx)
	ret0, _ := ret[0].([]string)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// LS indicates an expected call of LS
func (mr *MockStorageMockRecorder) LS(ctx interface{}) *gomock.Call {
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "LS", reflect.TypeOf((*MockStorage)(nil).LS), ctx)
}
