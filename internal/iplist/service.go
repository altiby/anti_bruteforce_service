package iplist

import (
	"context"
	"net"
)

type Service struct {
	storage            Storage
	timeLimitForUpdate int
	ipList             []string
}

func NewIPListService(storage Storage, timeLimitForUpdate int) *Service {
	s := &Service{storage: storage, timeLimitForUpdate: timeLimitForUpdate}
	return s
}

type Storage interface {
	Add(ctx context.Context, ip string) error
	Delete(ctx context.Context, ip string) error
	LS(ctx context.Context) ([]string, error)
}

func (s *Service) Add(ctx context.Context, ip string) error {
	err := s.storage.Add(ctx, ip)
	if err != nil {
		return err
	}
	_, err = s.LS(ctx)
	if err != nil {
		return err
	}
	return nil
}

func (s *Service) Delete(ctx context.Context, ip string) error {
	err := s.storage.Delete(ctx, ip)
	if err != nil {
		return err
	}
	_, err = s.LS(ctx)
	if err != nil {
		return err
	}
	return nil
}

func (s *Service) IPInList(ctx context.Context, ip string) (bool, error) {
	for _, val := range s.ipList {
		_, network, err := net.ParseCIDR(val)
		if err != nil {
			return false, err
		}
		addr := net.ParseIP(ip)
		if network.Contains(addr) {
			return true, nil
		}
	}
	return false, nil
}

func (s *Service) LS(ctx context.Context) ([]string, error) {
	data, err := s.storage.LS(ctx)
	if err != nil {
		return nil, err
	}
	s.ipList = data
	return data, err
}
