package iplist

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
)

func TestService_IPInList(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	storage := NewMockStorage(mockCtrl)
	ipList := []string{"192.168.0.0/24", "172.16.0.0/24"}
	storage.EXPECT().LS(context.Background()).Return(ipList, nil).AnyTimes()
	service := NewIPListService(storage, 0)
	serviceIPList, err := service.LS(context.Background())
	require.NoError(t, err)
	require.Equal(t, ipList, serviceIPList)

	t.Run("ip in list", func(t *testing.T) {
		inList, err := service.IPInList(context.Background(), "192.168.0.10")
		require.NoError(t, err)
		require.True(t, inList)

		inList, err = service.IPInList(context.Background(), "172.16.0.254")
		require.NoError(t, err)
		require.True(t, inList)
	})

	t.Run("ip not in list", func(t *testing.T) {
		inList, err := service.IPInList(context.Background(), "192.169.0.10")
		require.NoError(t, err)
		require.False(t, inList)

		inList, err = service.IPInList(context.Background(), "173.16.0.254")
		require.NoError(t, err)
		require.False(t, inList)
	})
}
