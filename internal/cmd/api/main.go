package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"

	"bitbucket.org/altiby/anti_bruteforce_service/internal/antibruteforce"
	"bitbucket.org/altiby/anti_bruteforce_service/internal/config"
	"bitbucket.org/altiby/anti_bruteforce_service/internal/db"
	"bitbucket.org/altiby/anti_bruteforce_service/internal/iplist"
	"bitbucket.org/altiby/anti_bruteforce_service/internal/redisstorage"
	"bitbucket.org/altiby/anti_bruteforce_service/internal/view/grpc/abf"
	"github.com/antelman107/net-wait-go/wait"
	"github.com/go-redis/redis/v8"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_logrus "github.com/grpc-ecosystem/go-grpc-middleware/logging/logrus"
	grpc_recovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	grpc_ctxtags "github.com/grpc-ecosystem/go-grpc-middleware/tags"
	"github.com/jinzhu/configor"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
	prefixed "github.com/x-cray/logrus-prefixed-formatter"
	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"
)

var (
	version     string
	versionFlag = flag.Bool("version", false, "app version")
)

var (
	cfg  config.Config
	conn *sqlx.DB
	rdb  *redis.Client
)

func main() {
	// app binary version
	flag.Parse()
	if *versionFlag {
		fmt.Println(version)
		return
	}

	if err := configor.Load(&cfg, "etc/config.yml"); err != nil {
		logrus.Fatal(err)
	}

	setupLogging()

	if !wait.New(
		wait.WithProto("tcp"),
		wait.WithWait(1*time.Second),
		wait.WithBreak(1*time.Second),
		wait.WithDeadline(30*time.Second),
		wait.WithDebug(true),
	).Do([]string{cfg.DBHost, cfg.Redis.Address}) {
		logrus.Error("db is not available")
		return
	}

	// postgres
	var err error
	conn, err = sqlx.Open("postgres", cfg.DB)
	if err != nil {
		logrus.Fatal(err)
	}

	conn.SetMaxIdleConns(10)
	conn.SetMaxOpenConns(10)

	driver, err := postgres.WithInstance(conn.DB, &postgres.Config{})
	if err != nil {
		logrus.Fatal(err)
	}
	m, err := migrate.NewWithDatabaseInstance(
		"file://migrations",
		"postgres", driver)
	if err != nil {
		logrus.Fatal(err)
	}

	err = m.Up()
	if err != nil && !errors.Is(err, migrate.ErrNoChange) {
		logrus.Fatal(err)
	}

	// redis
	rdb = redis.NewClient(&redis.Options{
		Addr:     cfg.Redis.Address,
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	runAsGRPC()

	terminate := make(chan os.Signal)
	signal.Notify(terminate, syscall.SIGINT, syscall.SIGTERM) //nolint
	s := <-terminate
	logrus.Infof("service closed with signal: %s", s)
}

func runAsGRPC() {
	// GRPC
	lis, err := net.Listen("tcp", fmt.Sprintf("0.0.0.0:%d", cfg.GRPCPort))
	if err != nil {
		logrus.Fatalf("failed to listen: %v", err)
	}

	opts := []grpc.ServerOption{
		grpc.StreamInterceptor(grpc_middleware.ChainStreamServer(
			grpc_ctxtags.StreamServerInterceptor(),
			grpc_logrus.StreamServerInterceptor(logrus.WithField("context", "grpc server")),
		)),
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
			grpc_ctxtags.UnaryServerInterceptor(),
			grpc_recovery.UnaryServerInterceptor(),
			grpc_logrus.UnaryServerInterceptor(logrus.WithField("context", "grpc server")),
		)),
		grpc.KeepaliveParams(keepalive.ServerParameters{
			MaxConnectionIdle: 5 * time.Minute,
		}),
	}

	blackListStorage := db.NewStorage(conn, db.IPListTypeBlackList)
	blackListService := iplist.NewIPListService(blackListStorage, cfg.IPListServiceUpdateInterval)
	_, err = blackListService.LS(context.Background()) // initial read
	if err != nil {
		logrus.Fatalf("failed read ip black list %v", err)
	}

	whitelistStorage := db.NewStorage(conn, db.IPListTypeWhiteList)
	whiteListService := iplist.NewIPListService(whitelistStorage, cfg.IPListServiceUpdateInterval)
	_, err = blackListService.LS(context.Background()) // initial read
	if err != nil {
		logrus.Fatalf("failed read ip white list %v", err)
	}

	rdbStorage := redisstorage.NewRDBStorage(rdb)
	abfService := antibruteforce.NewAntiBrutForcesService(rdbStorage,
		cfg.ABF.RequestOnIPPerMinute,
		cfg.ABF.RequestOnUserPerMinute,
		cfg.ABF.RequestOnPasswordPerMinute)
	grpcServer := grpc.NewServer(opts...)
	abf.RegisterABFServiceServer(grpcServer, abf.NewABFServer(abfService, blackListService, whiteListService))
	grpcServer.Serve(lis)
}

func setupLogging() {
	// only log the warning severity or above.
	logrus.SetLevel(logrus.WarnLevel)
	logrus.SetFormatter(&prefixed.TextFormatter{
		ForceColors:      true,
		DisableTimestamp: false,
		ForceFormatting:  true,
		FullTimestamp:    true,
		QuoteEmptyFields: true,
	})
	if logLevel, err := logrus.ParseLevel(cfg.LogLevel); err == nil {
		logrus.SetLevel(logLevel)
	}
}
