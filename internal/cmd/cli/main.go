package main

import (
	"context"
	"fmt"
	"os"
	"regexp"

	"bitbucket.org/altiby/anti_bruteforce_service/internal/view/grpc/abf"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	prefixed "github.com/x-cray/logrus-prefixed-formatter"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/emptypb"
)

var grpcClient abf.ABFServiceClient

func main() {
	setupLogging()
	app := setupCLI()

	abfServiceConnection, err := grpc.Dial("localhost:9090",
		grpc.WithInsecure(),
		grpc.WithBlock(),
	)
	if err != nil {
		logrus.Fatal(err)
	}
	grpcClient = abf.NewABFServiceClient(abfServiceConnection)
	defer abfServiceConnection.Close()

	if err := app.Run(os.Args); err != nil {
		logrus.Fatal(err)
	}
}

func isValidCIDR(cidr string) bool {
	match, _ := regexp.MatchString("^([0-9]{1,3}\\.){3}[0-9]{1,3}($|/(16|24))$", cidr)
	return match
}

func setupCLI() *cli.App {
	return &cli.App{
		Name:  "cli",
		Usage: "cli interface for abf service",
		Commands: []*cli.Command{
			{ //nolint
				Name:    "whitelist",
				Usage:   "cli interface for white list service",
				Aliases: []string{"wl"},
				Subcommands: []*cli.Command{
					{
						Name:  "add",
						Usage: "add a new cidr to white list",
						Action: func(ctx *cli.Context) error {
							cidr := ctx.Args().First()
							if !isValidCIDR(cidr) {
								return cli.Exit("not valid cidr", 1)
							}
							_, err := grpcClient.AddToWhiteList(context.Background(), &abf.IPRequest{Ip: cidr})
							if err != nil {
								return cli.Exit(err.Error(), 1)
							}
							return nil
						},
					},
					{
						Name:  "remove",
						Usage: "remove an existing cidr to white list",
						Action: func(ctx *cli.Context) error {
							cidr := ctx.Args().First()
							if !isValidCIDR(cidr) {
								return cli.Exit("not valid cidr", 1)
							}
							_, err := grpcClient.DelFromWhiteList(context.Background(), &abf.IPRequest{Ip: cidr})
							if err != nil {
								return cli.Exit(err.Error(), 1)
							}
							return nil
						},
					},
					{
						Name:  "ls",
						Usage: "list an existing cidr in white list",
						Action: func(ctx *cli.Context) error {
							list, err := grpcClient.LSBWhiteList(context.Background(), &emptypb.Empty{})
							if err != nil {
								return cli.Exit(err.Error(), 1)
							}
							for _, ip := range list.Ip {
								fmt.Println(ip)
							}
							return nil
						},
					},
				},
			},
			{ // nolint
				Name:    "blacklist",
				Usage:   "cli interface for black list service",
				Aliases: []string{"bl"},
				Subcommands: []*cli.Command{
					{
						Name:  "add",
						Usage: "add a new cidr to black list",
						Action: func(ctx *cli.Context) error {
							cidr := ctx.Args().First()
							if !isValidCIDR(cidr) {
								return cli.Exit("not valid cidr", 1)
							}
							_, err := grpcClient.AddToBlackList(context.Background(), &abf.IPRequest{Ip: cidr})
							if err != nil {
								return cli.Exit(err.Error(), 1)
							}
							return nil
						},
					},
					{
						Name:  "remove",
						Usage: "remove an existing cidr to black list",
						Action: func(ctx *cli.Context) error {
							cidr := ctx.Args().First()
							if !isValidCIDR(cidr) {
								return cli.Exit("not valid cidr", 1)
							}
							_, err := grpcClient.DelFromBlackList(context.Background(), &abf.IPRequest{Ip: cidr})
							if err != nil {
								return cli.Exit(err.Error(), 1)
							}
							return nil
						},
					},
					{
						Name:  "ls",
						Usage: "list an existing cidr in white list",
						Action: func(ctx *cli.Context) error {
							list, err := grpcClient.LSBlackList(context.Background(), &emptypb.Empty{})
							if err != nil {
								return cli.Exit(err.Error(), 1)
							}
							for _, ip := range list.Ip {
								fmt.Println(ip)
							}
							return nil
						},
					},
				},
			},
		},
	}
}

func setupLogging() {
	// only log the warning severity or above.
	logrus.SetLevel(logrus.WarnLevel)
	logrus.SetFormatter(&prefixed.TextFormatter{
		ForceColors:      true,
		DisableTimestamp: false,
		ForceFormatting:  true,
		FullTimestamp:    true,
		QuoteEmptyFields: true,
	})
	if logLevel, err := logrus.ParseLevel("info"); err == nil {
		logrus.SetLevel(logLevel)
	}
}
