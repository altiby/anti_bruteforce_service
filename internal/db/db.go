package db

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/jmoiron/sqlx"
)

var ErrNothingToUpdate = errors.New("nothing to update")

type IPListType string

const (
	IPListTypeBlackList IPListType = "blacklist"
	IPListTypeWhiteList IPListType = "whitelist"
)

type Storage struct {
	ext      sqlx.ExtContext
	listType IPListType
}

func (s *Storage) getStorageTableName(ipListType IPListType) string {
	switch ipListType {
	case IPListTypeBlackList:
		return "blacklist"
	case IPListTypeWhiteList:
		return "whitelist"
	}
	panic(fmt.Sprintf("uncnown list type %s", ipListType))
}

func (s *Storage) Add(ctx context.Context, ip string) error {
	tableName := s.getStorageTableName(s.listType)
	_, err := s.ext.ExecContext(ctx, fmt.Sprintf("INSERT INTO %s (ip) VALUES ($1) ON CONFLICT DO NOTHING", tableName), ip)
	return err
}

func (s *Storage) Delete(ctx context.Context, ip string) error {
	tableName := s.getStorageTableName(s.listType)
	result, err := s.ext.ExecContext(ctx, fmt.Sprintf("DELETE FROM %s WHERE ip=$1", tableName), ip)
	if err != nil {
		return err
	}
	return checkAffected(result)
}

func (s *Storage) LS(ctx context.Context) ([]string, error) {
	var ipList []string
	tableName := s.getStorageTableName(s.listType)
	err := sqlx.SelectContext(ctx, s.ext, &ipList, fmt.Sprintf("SELECT ip FROM %s", tableName))
	if err != nil {
		return nil, err
	}
	return ipList, nil
}

func NewStorage(ext sqlx.ExtContext, listType IPListType) *Storage {
	return &Storage{ext: ext, listType: listType}
}

func checkAffected(res sql.Result) error {
	affected, err := res.RowsAffected()
	if err != nil {
		return err
	}

	if affected == 0 {
		return ErrNothingToUpdate
	}

	return nil
}
