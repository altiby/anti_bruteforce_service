package db

import (
	"context"
	"flag"
	"os"
	"testing"

	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/require"
)

var conn *sqlx.DB

func TestMain(m *testing.M) {
	flag.Parse()
	if testing.Short() {
		return
	}

	migrationsDir := os.Getenv("GOPATH") + "/src/bitbucket.org/altiby/anti_bruteforce_service/migrations"
	DockertestMain(m, migrationsDir, func(c *sqlx.DB) {
		conn = c
	})
}

func cleanUp() {
	conn.MustExec("DELETE FROM whitelist")
	conn.MustExec("DELETE FROM blacklist")
}

func TestStorage_Add(t *testing.T) {
	defer cleanUp()
	storageBL := NewStorage(conn, IPListTypeBlackList)
	require.NoError(t, storageBL.Add(context.Background(), "192.168.1.0/24"))
	require.NoError(t, storageBL.Add(context.Background(), "192.168.1.0/24"))
	storageWL := NewStorage(conn, IPListTypeWhiteList)
	require.NoError(t, storageWL.Add(context.Background(), "192.168.1.0/24"))
	require.NoError(t, storageWL.Add(context.Background(), "192.168.1.0/24"))
}

func TestStorage_Delete(t *testing.T) {
	defer cleanUp()
	storageBL := NewStorage(conn, IPListTypeBlackList)
	require.NoError(t, storageBL.Add(context.Background(), "192.168.1.0/24"))
	require.NoError(t, storageBL.Delete(context.Background(), "192.168.1.0/24"))
	storageWL := NewStorage(conn, IPListTypeWhiteList)
	require.NoError(t, storageWL.Add(context.Background(), "192.168.1.0/24"))
	require.NoError(t, storageWL.Delete(context.Background(), "192.168.1.0/24"))
}

func TestStorage_LS(t *testing.T) {
	defer cleanUp()
	storageBL := NewStorage(conn, IPListTypeBlackList)
	require.NoError(t, storageBL.Add(context.Background(), "192.168.1.0/24"))
	require.NoError(t, storageBL.Add(context.Background(), "192.168.1.0/24"))
	require.NoError(t, storageBL.Add(context.Background(), "192.168.1.1/24"))
	ipList, err := storageBL.LS(context.Background())
	require.NoError(t, err)
	require.Len(t, ipList, 2)
	require.Equal(t, ipList[0], "192.168.1.0/24")
	require.Equal(t, ipList[1], "192.168.1.1/24")
	require.NoError(t, storageBL.Delete(context.Background(), "192.168.1.0/24"))
	ipList, err = storageBL.LS(context.Background())
	require.NoError(t, err)
	require.Len(t, ipList, 1)
	require.Equal(t, ipList[0], "192.168.1.1/24")
}
