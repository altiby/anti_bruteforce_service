package db

import (
	"fmt"
	"log"
	"os"
	"testing"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"

	// nolint
	"github.com/ory/dockertest"

	// nolint
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/jmoiron/sqlx"

	// nolint
	_ "github.com/lib/pq"
)

func DockertestMain(m *testing.M, migrationsDir string, up func(conn *sqlx.DB)) {
	var (
		err  error
		conn *sqlx.DB
	)

	pool, err := dockertest.NewPool("")
	if err != nil {
		log.Fatalf("Could not connect to docker pool: %s", err)
	}

	// pulls an image, creates a container based on it and runs it
	resource, err := pool.Run("mdillon/postgis", "10-alpine", []string{})
	if err != nil {
		log.Fatalf("Could not start resource: %s", err)
	}

	// exponential backoff-retry, because the application in the container might not be ready to accept connections yet
	if err := pool.Retry(func() error {
		writeConnectionString := fmt.Sprintf(
			"user=postgres dbname=postgres sslmode=disable port=%s", resource.GetPort("5432/tcp"))
		conn, err = sqlx.Open("postgres", writeConnectionString)
		if err != nil {
			return err
		}
		return conn.Ping()
	}); err != nil {
		log.Fatalf("Could not connect to docker: %s", err)
	}

	driver, err := postgres.WithInstance(conn.DB, &postgres.Config{})
	if err != nil {
		log.Fatal(err)
	}

	mg, err := migrate.NewWithDatabaseInstance(
		"file://"+migrationsDir,
		"postgres", driver)
	if err != nil {
		log.Fatal(err)
	}

	err = mg.Up()
	if err != nil {
		log.Fatal(err)
	}

	up(conn)

	code := m.Run()

	// You can't defer this because os.Exit doesn't care for defer
	if err := pool.Purge(resource); err != nil {
		log.Fatalf("Could not purge resource: %s", err)
	}

	os.Exit(code)
}
