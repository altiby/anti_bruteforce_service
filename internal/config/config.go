package config

type Config struct {
	GRPCPort                    int       `yaml:"grpc_port"` //nolint:tagliatelle
	LogLevel                    string    `yaml:"log_level"` //nolint:tagliatelle
	ABF                         ABF       `yaml:"abf"`
	DB                          string    `yaml:"db" required:"true"`
	DBHost                      string    `yaml:"db_host" required:"true"` //nolint:tagliatelle
	Redis                       RedisConf `yaml:"redis"`
	IPListServiceUpdateInterval int       `yaml:"ip_list_service_update_interval"` //nolint:tagliatelle
}

type ABF struct {
	RequestOnIPPerMinute       int64 `yaml:"request_on_ip_per_minute"`       //nolint:tagliatelle
	RequestOnUserPerMinute     int64 `yaml:"request_on_user_per_minute"`     //nolint:tagliatelle
	RequestOnPasswordPerMinute int64 `yaml:"request_on_password_per_minute"` //nolint:tagliatelle
}

type RedisConf struct {
	Address string `yaml:"address"`
}
