FROM golang:1.16-alpine AS build
RUN apk update && apk add ca-certificates
RUN apk add bash
RUN apk add make
WORKDIR /go/src/project/
COPY . /go/src/project/
RUN make build

FROM alpine:3.8

RUN apk update && apk add ca-certificates
RUN apk add bash
COPY --from=build /go/src/project/api /app/
COPY ./etc /app/etc/
COPY ./migrations /app/migrations/
WORKDIR /app
EXPOSE 9090
CMD ["/app/api"]
#ENTRYPOINT ["/app/api"]